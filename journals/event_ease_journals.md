## Sun Mar 18, 2023

Today I worked on:

* Project setup

I'm redoing and refactoring the conference-go project from my hackreactor bootcamp in order to combine and improve the original project that was split amongst three respositories. The idea is to better track the project under one git history and document my successes and struggles along the way. I also want to gain a better understanding on unmonolithing, switching from a sqlite db to a postgres db, and refactoring the front end to use hook based components rather than class based components.


## Sun Mar 19, 2023

Today I worked on:

* Created initial view functions
* Refactored JSON responses to use an encoding function
* Restfulized the project

Using the scaffold provided, I updated the view functions to return JSON responses when called by a client. The first iteration used manually configured jsonresponse objects. They were then refactored to use a json encoder function that inherited properties from the python json encoder class. Edge cases for queryset objects and datetime objects were handled using additional encoder classes which are then inherited by the main model encoder.

I also restfulized the endpoints. Additional HTTP methods were added including POST, PUT, and DELETE.

Leaving a general reminder here to eliminate the CSRF middleware in the base django scaffold to avoid CSRF errors prior to restfulizing.


## Mon Mar 20, 2023

Today I worked on:

* Finished restfulizing the django model endpoints
* Added 3rd party API integration with Pexels and Open Weather

I added the remaining endpoint functionality to add the POST, PUT, and DELETE methods on all endpoints. I also worked on adding 3rd party API integration with the python requests library to use Pexels and Open Weather. I was able to integrate the api keys in an environment file using the django-environ module.


## Wed Mar 22, 2023

Today I wored on:

* Broke the project into microservices
* Added a poller for conference data
* Added a queue to handle presentation approvals/rejections
* Added a pub/sub to handle active accounts

Today was a large day in the project. I took the project and converted it to a structure which has a monolith base and several microservices. The Attendees microservice handles the creation and assignment of attendees to conferences. I added a poller to scrape conference data from the monolith. In addition, I added a consumer to handle account information from the monolith accounts app to accept publishes when accounts are created. This changes attendees account status between inactive and active. The last thing I did was create a presentation microservice. Its job is to handle presentation approvals and rejections. When a presentation is reviewed and its status changed, the monolith produces to a queue which is then handled by a consumer in the microservice. The built in django mailer function was used to send out approval/rejection emails. Which I was able to see using a test mail service using MailHog.

I ran into challenges setting up the poller. I wasn't aware that print statements do not run in chronjobs. This made debugging a syntax error difficult.

I also ran into a bug setting up the queue. I had a typo in my queue name between the declaration and consumer which threw an error. Once I slowed down, read the error, and back tracked I was able to find the issue.

The last challenge I had to fix was implementing someone elses code. When setting up the consumer for account data in the attendees microservice I had to implement data from view functions that were written by others in the monolith. This caused confusion when I was analyzing how the data should be parsed. Ultimately I had to revert and old migration and correct a mistake I made when originally setting up the AccountVO model in the microservice.


## Fri Mar 24, 2023

Today I worked on:

* Converting the project to postgres
* Creating initial front end

Converting to postgres was difficult. I had to refresh on how to implement and swap from sqlite. I was able to manage converting but it did take some time. Some of my big take aways was understanding how docker/django accesses the volumes and databases. I was able to speed up the process by using a script file provided in another template. I was also able to use the "loaddata" command in django to quickly load conference data back into the dbs once they were converted. I realized that I had to manually add image data because the location instances weren't made using the view functions. I was able to make an endpoint call in Insomnia and add header/query info to mimic what my api functions were doing. I was able to manually enter image data into my database using pgadmin.

Using some provided starter code I implemented an initial javascript and html front end for the application.



## Sun Mar 26, 2023

Today I worked on:

* Landing page
* Location form

I created front end fetch calls to the monolith in order to get State model data to make a location using a bootstrap form. I also created card features to hold conference data on the landing page. I coded in conditional logic to hold placeholder cards while the conference data loads on the site. Once the card is loaded, the placeholder is hidden.


## Mon Mar 27, 2023

Today I worked on:

* Nav Bar
* New Conference Form
* New Presentation Form

I added additional link tags for additional front end pages. This included a new conference form and new presentation form. Using bootstrap as a base I was able to design forms which hold locations and conference data. Manipulating the data in order to correctly fit with the back end endpoints was challenging. In some cases the way the endpoint url was structured influenced how the form was setup. I had to make sure that the correct values were being collected and sent back to the API.


## Tues Mar 28, 2023

Today I worked on:

* Attend Conference Form
* JWT Token Auth
* Conditional Nav Bar for logged in users

I added a button to the nav bar and main page which takes the user to a form to attend a conference. The attend conference form has a built in conditional which activates once the form is correctly submitted. I also implemented JWT tokens so that the front end can log in and access information via cookie. I struggled with implementing the stretch goal for a conditional nav bar. I had to debug my code to be able to handle a promise object when obtaining the cookie payload information. Once I had that handled I was able to parse and access the data needed to make the nav bar conditionals function. I ended the day by getting the react application started.
