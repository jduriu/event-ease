from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    '''
        if the object to decode is the same class as what's in the
        model property, then
        * create an empty dictionary that will hold the property names
            as keys and the property values as values
        * if o has the attribute get_api_url
                then add its reutn value to the dictionary
                with the key "href"
        * for each name in the properties list
            * get the value of that property from the model instance
                given just the property name
            * if the property is in encoders
                use the encoder that is listed to serialize the data
            * put it into the dictionary with that property name as
                the key
        * add any data that is not covered in the properties or encoders
        * return the dictionary
        otherwise,
            return super().default(o)  # From the documentation
    '''
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            di = {}
            if hasattr(o, "get_api_url"):
                di["href"] = o.get_api_url()
            for prop in self.properties:
                value = getattr(o, prop)
                if prop in self.encoders:
                    encoder = self.encoders[prop]
                    value = encoder.default(value)
                di[prop] = value
            di.update(self.get_extra_data(o))
            return di
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
