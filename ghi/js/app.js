function createPlaceholder(count) {
    return `
    <div class="card my-4 shadow" id="placeholder-${count}" aria-hidden="true">
        <img src="..." class="card-img-top" alt="LOADING">
        <div class="card-body">
            <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
            </h5>
            <h6 class="card-subtitle mb-2 placeholder-glow">
                <span class="placeholder col-6"></span>
            </h6>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
            </p>
        </div>
        <div class="card-footer placeholder-glow">
            <span class="placeholder col-8"></span>
        </div>
    </div>
    `
}


function createCard(name, description, pictureUrl, start, end, location_name) {
    return `
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location_name}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${start} - ${end}
        </div>
    `
}

function handleAlert(error) {
    return `
    <div class="alert alert-info" role="alert>
        <h4 class="alert-heading">Oh no!</h4>
        <p>Something went wrong loading your current conferences</p>
        <hr>
        <p>
        Try refreshing the page or contact EventEase to resolve the issue
        </p>
    </div>
    `
}



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'
    try {
        const response = await fetch(url)
        if (!response.ok) {
            const alertMessage = handleAlert(e)
            const conferenceContainer = document.querySelector('.container')
            conferenceContainer.innerHTML = alertMessage
        } else {
            const data = await response.json()
            let count = 0
            while (count < data.conferences.length) {
                const placeholder = createPlaceholder(count)
                const column = document.querySelector(`#col${count % 3}`)
                column.innerHTML += placeholder
                count += 1
            }

            count = 0
            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const pictureUrl = details.conference.location.picture_url
                    const location_name = details.conference.location.name
                    const description = details.conference.description
                    const start = new Date(details.conference.starts)
                    const start_formatted = start.toLocaleDateString('en-US')
                    const end = new Date(details.conference.ends)
                    const end_formatted = end.toLocaleDateString('en-US')
                    const html = createCard(title, description, pictureUrl, start_formatted, end_formatted, location_name, count)
                    const card = document.querySelector(`#placeholder-${count}`)
                    card.innerHTML = html
                    count += 1
                }
            }
        }

    } catch (e) {
        console.log(e)
        const alertMessage = handleAlert(e)
        const conferenceContainer = document.querySelector('.container')
        conferenceContainer.innerHTML = alertMessage
    }

});
