
window.addEventListener('DOMContentLoaded', async () => {
    const conferencesUrl = "http://localhost:8000/api/conferences/"

    try {
        const response = await fetch(conferencesUrl)

        if (response.ok) {
            const data = await response.json()
            for (let conference of data.conferences) {
                const conferenceOption = `
                    <option value="${conference.id}">
                        ${conference.name}
                    </option>
                `
                const conferenceDropdown = document.querySelector('#conference')
                conferenceDropdown.innerHTML += conferenceOption
            }
        }

    } catch (e) {
        console.log(e)
    }


    const formTag = document.getElementById("create-presentation-form")
    formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const formObj = Object.fromEntries(formData)
        const conference_id = formObj["conference"]
        const json = JSON.stringify(formObj)
        const presentationUrl = `http://localhost:8000/api/conferences/${conference_id}/presentations/`
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(presentationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newPresentation = await response.json()
            console.log(newPresentation)
        }
    })
})
