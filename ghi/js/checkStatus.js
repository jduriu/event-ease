

const payloadCookie = await cookieStore.get('jwt_access_payload')

if (payloadCookie) {
    const encodedPayload = JSON.parse(payloadCookie.value)
    const decodedPayload = atob(encodedPayload)
    const payload = JSON.parse(decodedPayload)
    const perms = payload.user.perms
    if (perms.includes('events.add_conference')) {
        const navTag = document.getElementById('newConferenceLink')
        navTag.classList.remove('d-none')
    }
    if (perms.includes('events.add_location')) {
        const navTag = document.getElementById('newLocationLink')
        navTag.classList.remove('d-none')
    }
} else {
    console.log("Couldn't find cookie!")
}
