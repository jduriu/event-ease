window.addEventListener('DOMContentLoaded', async () => {
    const conferencesUrl = "http://localhost:8000/api/conferences/"

    try {
        const response = await fetch(conferencesUrl)
        const conferenceDropdown = document.querySelector('#conference')

        if (response.ok) {
            const data = await response.json()
            for (let conference of data.conferences) {
                const conferenceOption = `
                    <option value="${conference.id}">
                        ${conference.name}
                    </option>
                `
                conferenceDropdown.innerHTML += conferenceOption
            }
            const spinner = document.getElementById('loading-conference-spinner')
            spinner.classList.add('d-none')
            conferenceDropdown.classList.remove('d-none')
        }

    } catch (e) {
        console.log(e)
    }


    const formTag = document.getElementById("create-attendee-form")
    formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const formObj = Object.fromEntries(formData)
        const conference_id = formObj["conference"]
        console.log(conference_id)
        formObj["conference"] = `/api/conferences/${conference_id}/`
        const json = JSON.stringify(formObj)
        const attendeeUrl = `http://localhost:8001/api/attendees/`
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(attendeeUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newAttendee = await response.json()
            console.log(newAttendee)
            formTag.classList.add('d-none')
            const successMessage = document.getElementById('success-message')
            successMessage.classList.remove('d-none')
        }
    })
})
