
window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = "http://localhost:8000/api/locations/"

    try {
        const response = await fetch(locationUrl)

        if (response.ok) {
            const data = await response.json()
            for (let location of data.locations) {
                const locationOption = `
                    <option value="${location.id}">
                        ${location.name}
                    </option>
                `
                const locationDropdown = document.querySelector('#location')
                locationDropdown.innerHTML += locationOption
            }
        }

    } catch (e) {
        console.log(e)
    }


    const formTag = document.getElementById("create-conference-form")
    formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newConference = await response.json()
            console.log(newConference)
        }
    })
})
