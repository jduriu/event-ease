
window.addEventListener('DOMContentLoaded', async () => {
    const stateUrl = "http://localhost:8000/api/states"

    try {
        const response = await fetch(stateUrl)

        if (response.ok) {
            const data = await response.json()
            for (let state of data.states) {
                const stateOption = `
                    <option value="${state.abbreviation}">
                        ${state.name}
                    </option>
                `
                const stateDropdown = document.querySelector('#state')
                stateDropdown.innerHTML += stateOption
            }
        }

    } catch (e) {
        console.log(e)
    }


    const formTag = document.getElementById("create-location-form")
    formTag.addEventListener("submit", async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newLocation = await response.json()
            console.log(newLocation)
        }
    })
})
