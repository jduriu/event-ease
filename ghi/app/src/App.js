import Nav from './Nav'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConference from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm'
import PresentationForm from './PresentationForm'
import MainPage from './MainPage'
import './App.css';
import React, { useState, useEffect } from 'react'
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'



function App() {

  const [attendees, setAttendees] = useState([])
  async function loadAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/')
    if (response.ok) {
      const data = await response.json()
      setAttendees(data.attendees)
    } else {
      console.error(response)
    }
  }

  const [states, setStates] = useState([])
  async function loadStates() {
    const response = await fetch('http://localhost:8000/api/states')
    if (response.ok) {
      const data = await response.json()
      setStates(data.states)
    } else {
      console.error(response)
    }
  }

  const [locations, setLocations] = useState([])
  async function loadLocations() {
    const response = await fetch('http://localhost:8000/api/locations/')
    if (response.ok) {
      const data = await response.json()
      setLocations(data.locations)
    } else {
      console.error(response)
    }
  }

  const [conferences, setConferences] = useState([])
  async function loadConferences() {
    const response = await fetch('http://localhost:8000/api/conferences')
    if (response.ok) {
      const data = await response.json()
      setConferences(data.conferences)
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadAttendees()
    loadStates()
    loadLocations()
    loadConferences()
  }, [])


  if (attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="attendees" element={<AttendeesList attendees={attendees} />} />
        <Route path="attendees/new" element={<AttendConference conferences={conferences} />} />
        <Route path="locations/new" element={<LocationForm states={states} />} />
        <Route path="conferences/new" element={<ConferenceForm locations={locations} />} />
        <Route path="presentations/new" element={<PresentationForm conferences={conferences} />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
