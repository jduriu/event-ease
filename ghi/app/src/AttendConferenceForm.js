import React, { useState } from 'react'

function AttendConference({ conferences }) {

    const [submitted, setSubmitted] = useState(false)

    const [conference, setConference] = useState("")
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")

    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.conference = conference
        data.name = name
        data.email = email

        const attendeesUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(attendeesUrl, fetchConfig)
        if (response.ok) {
            const newAttendee = await response.json()
            console.log(newAttendee)
            setConference("")
            setName("")
            setEmail("")
            setSubmitted(true)
        }


    }

    return (
        <div className="container">
            <div className="my-5">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="Conference Go Logo" />
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                {submitted ?
                                    <div className="alert alert-success mb-0" id="success-message">
                                        Congratulations! You're all signed up!
                                    </div> :
                                    <form onSubmit={handleSubmit} id="create-attendee-form">
                                        <h1 className="card-title">It's Conference Time!</h1>
                                        <p className="mb-3">
                                            Please choose which conference
                                            you'd like to attend.
                                        </p>
                                        {conferences ?
                                            <div className="mb-3">
                                                <select value={conference} onChange={handleConferenceChange} name="conference" id="conference" className="form-select" required>
                                                    <option value="">Choose a conference</option>
                                                    {conferences.map(conference => {
                                                        return (
                                                            <option value={conference.href} key={conference.href}>
                                                                {conference.name}
                                                            </option>
                                                        )
                                                    })}
                                                </select>
                                            </div> :
                                            <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                                <div className="spinner-grow text-secondary" role="status">
                                                    <span className="visually-hidden">Loading...</span>
                                                </div>
                                            </div>
                                        }
                                        <p className="mb-3">
                                            Now, tell us about yourself.
                                        </p>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input value={name} onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                                                    <label htmlFor="name">Your full name</label>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-floating mb-3">
                                                    <input value={email} onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                                                    <label htmlFor="email">Your email address</label>
                                                </div>
                                            </div>
                                        </div>
                                        <button className="btn btn-lg btn-primary">I'm going!</button>
                                    </form>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AttendConference
