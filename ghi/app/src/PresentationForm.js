import React, { useState } from 'react'

function PresentationForm({ conferences }) {

    const [presenterName, setPresenterName] = useState("")
    const [email, setEmail] = useState("")
    const [companyName, setCompanyName] = useState("")
    const [title, setTitle] = useState("")
    const [synopsis, setSynopsis] = useState("")
    const [conference, setConference] = useState("")


    const handlePresenterNameChange = (event) => {
        const value = event.target.value
        setPresenterName(value)
    }
    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value
        setCompanyName(value)
    }
    const handleTitleChange = (event) => {
        const value = event.target.value
        setTitle(value)
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value
        setSynopsis(value)
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.presenter_name = presenterName
        data.presenter_email = email
        data.company_name = companyName
        data.title = title
        data.synopsis = synopsis


        const presentationsUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(presentationsUrl, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)

            setPresenterName("")
            setEmail("")
            setCompanyName("")
            setTitle("")
            setSynopsis("")
            setConference("")
        }


    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input value={presenterName} onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={email} onChange={handleEmailChange} placeholder="Email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company name" required type="text" name="company_name" id="company_name" className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea value={synopsis} onChange={handleSynopsisChange} required type="text" name="synopsis" id="synopsis" className="form-control" rows="3"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <select value={conference} onChange={handleConferenceChange} required id="conference" className="form-select" name="conference">
                                    <option value="">Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option value={conference.id} key={conference.id}>
                                                {conference.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PresentationForm
